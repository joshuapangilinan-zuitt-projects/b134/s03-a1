USE blog_db;

INSERT INTO users (email, password, datetime_created)
VALUES ("johnsmith@gmail.com", "passwordA", "2021-1-1 1"),
("juandelacruz@gmail.com", "passwordB", "2021-1-1 2"),
("janesmith@gmail.com", "passwordC", "2021-1-1 3"),
("mariadelacruz@gmail.com", "passwordD", "2021-1-1 4"),
("johndoe@gmail.com", "passwordE", "2021-1-1 5");

INSERT INTO posts (author_id, title, content, datetime_posted)
VALUES (1, "First Code", "Hello World!", "2021-1-2 1"),
(1, "Second Code", "Hello Earth!", "2021-1-2 2"),
(2, "Third Code", "Welcome to Mars!", "2021-1-2 3"),
(4, "Fourth Code", "Bye bye solar system!", "2021-1-2 4");

SELECT * 
FROM posts 
WHERE author_id = 1;

SELECT email, datetime_created 
FROM users;

UPDATE posts 
SET content = "Hello to the people of the Earth!"
WHERE id = 2;

DELETE FROM users
WHERE email = "johndoe@gmail.com";